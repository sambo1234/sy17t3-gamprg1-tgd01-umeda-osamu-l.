// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Damage.generated.h"

UCLASS()
class ENDLESSRUNNER_API ADamage : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADamage();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Damage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 Health;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
	int32 MaxHealth;

	UFUNCTION(BlueprintCallable)
	void takeDamage(float value);
};
