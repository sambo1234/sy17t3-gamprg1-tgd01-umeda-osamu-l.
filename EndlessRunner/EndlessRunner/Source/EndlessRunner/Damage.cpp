// Fill out your copyright notice in the Description page of Project Settings.

#include "Damage.h"


// Sets default values
ADamage::ADamage()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void ADamage::BeginPlay()
{
	Super::BeginPlay();
	Health = MaxHealth;
}

// Called every frame
void ADamage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ADamage::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ADamage::takeDamage(float value)
{
	if (value < 0) return;
	Health -= value;

	if (Health <= 0)
	{
		Health = 0;
	}
}

