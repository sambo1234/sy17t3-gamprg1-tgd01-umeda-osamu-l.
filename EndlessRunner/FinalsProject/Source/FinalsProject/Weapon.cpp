// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"


// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	Ammo = MaxAmmo;
}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeapon::FireWeapon(float value)
{
	if (value < 0) return;
	Ammo -= value;

	if (Ammo > MaxAmmo)
	{
		Ammo = MaxAmmo;
	}
}

void AWeapon::AmmoPickup(float value)
{
	if (value < 0) return;
	Ammo += value;

	if (Ammo > MaxAmmo)
	{
		Ammo = MaxAmmo;
	}
}



