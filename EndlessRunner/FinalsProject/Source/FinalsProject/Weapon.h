// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

UCLASS()
class FINALSPROJECT_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
		int32 Damage;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
		int32 FireRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int32 MaxDistance;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
		int32 Ammo;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
		int32 MaxAmmo;

	UFUNCTION(BlueprintCallable)
		void FireWeapon(float value);
	
	UFUNCTION(BlueprintCallable)
		void AmmoPickup(float value);
};
