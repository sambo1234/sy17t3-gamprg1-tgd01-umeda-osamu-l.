// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TileOutbreak.generated.h"

UCLASS()
class FINALSPROJECT_API ATileOutbreak : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileOutbreak();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleDefaultsOnly, Category = Tile)
		class UStaticMeshComponent* Floor;

	UPROPERTY(VisibleDefaultsOnly, Category = Tile)
		class UStaticMeshComponent* Wall1;

	UPROPERTY(VisibleDefaultsOnly, Category = Tile)
		class UStaticMeshComponent* Wall2;

	UPROPERTY(VisibleDefaultsOnly, Category = Scene)
		class USceneComponent* Scene;
	
};
