// Fill out your copyright notice in the Description page of Project Settings.

#include "Hazard.h"
#include "Classes/Components/StaticMeshComponent.h"
#include "Classes/Components/SceneComponent.h"
//#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

// Sets default values
AHazard::AHazard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = Scene;

	Hazard = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BoxComponent"));
	Hazard->AttachTo(Scene);

	/*static ConstructorHelpers::FObjectFinder<UClass> HeroClassFinder(TEXT("Blueprint'/Game/Actor/Hero.Hero_C'"));

	Hero = HeroClassFinder.Object;*/
}

// Called when the game starts or when spawned
void AHazard::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHazard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

