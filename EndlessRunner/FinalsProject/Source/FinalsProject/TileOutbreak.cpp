// Fill out your copyright notice in the Description page of Project Settings.

#include "TileOutbreak.h"
#include "Classes/Components/BoxComponent.h"
#include "Classes/Components/StaticMeshComponent.h"
#include "Classes/Components/SceneComponent.h"




// Sets default values
ATileOutbreak::ATileOutbreak()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = Scene;

	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BoxComponent"));
	Floor->AttachTo(Scene);
	Floor->SetRelativeScale3D(FVector(20.0f, 10.0f, 0.1f));
	Floor->SetRelativeLocation(FVector(500.0f, -0.000001f, 0.0f));
	

	Wall1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall1"));
	Wall1->AttachTo(Scene);
	Wall1->SetRelativeScale3D(FVector(20.25f, 0.1f, 2.0f));
	Wall1->SetRelativeLocation(FVector(500.0f, -500.0f, 0.0f));

	Wall2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall2"));
	Wall2->AttachTo(Scene);
	Wall2->SetRelativeScale3D(FVector(20.25f, 0.1f, 2.0f));
	Wall2->SetRelativeLocation(FVector(500.0f, 500.0f, 0.0f));

	
}

// Called when the game starts or when spawned
void ATileOutbreak::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATileOutbreak::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

