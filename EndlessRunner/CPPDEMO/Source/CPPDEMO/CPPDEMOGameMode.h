// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CPPDEMOGameMode.generated.h"

UCLASS(minimalapi)
class ACPPDEMOGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACPPDEMOGameMode();
};



